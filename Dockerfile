FROM r-base:latest

MAINTAINER Vish Vishvanath "vishvish@users.noreply.github.com"

RUN apt-get update && apt-get install -y -t unstable \
    apt-utils \
    gdebi-core \
    libcairo2-dev/unstable \
    libcurl4-gnutls-dev \
    libxt-dev \
    pandoc \
    pandoc-citeproc \
    sudo

RUN wget --no-verbose https://s3.amazonaws.com/rstudio-shiny-server-os-build/ubuntu-12.04/x86_64/VERSION && \
    VERSION=$(cat VERSION)  && \
    wget --no-verbose "https://s3.amazonaws.com/rstudio-shiny-server-os-build/ubuntu-12.04/x86_64/shiny-server-$VERSION-amd64.deb" -O ss-latest.deb && \
    gdebi -n ss-latest.deb && \
    rm -f version.txt ss-latest.deb && \
    R -e "install.packages(c('shiny', 'rmarkdown'), repos='https://cran.rstudio.com/')" && \
    cp -R /usr/local/lib/R/site-library/shiny/examples/* /srv/shiny-server/ && \
    rm -rf /var/lib/apt/lists/*

EXPOSE 3838

COPY entrypoint.sh /usr/bin/entrypoint.sh
RUN chmod 755 /usr/bin/entrypoint.sh

CMD ["/usr/bin/entrypoint.sh"]
